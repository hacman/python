import unittest
import random
import sys
import struct

from test.test_support import (
    _4G, import_module, bigmemtest
)

lzma = import_module('lzma')
from lzma import LZMADecompressor

class CompressorDecompressorTestCase(unittest.TestCase):
    @bigmemtest(minsize=_4G + 100, memuse=3)
    def test_decompressor_bigmem(self, size):
        lzd = LZMADecompressor()
        blocksize = 10 * 1024 * 1024
        block = struct.pack('P' * (blocksize / 4),
                            *(random.randint(-sys.maxint, sys.maxint)
                             for i in xrange(blocksize/4))
                           )
        try:
            input = block * (size / blocksize + 1)
            block = None
            cdata = lzma.compress(input)
            ddata = lzd.decompress(cdata)
            self.assertEqual(ddata, input)
        finally:
            input = cdata = ddata = None


if __name__ == "__main__":
    unittest.main()
