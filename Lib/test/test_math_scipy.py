import unittest
import math
import os

EPS = 5e-6
NAN = float('nan')
INF = float('inf')
NINF = float('-inf')

class SciPyTest(unittest.TestCase):
    def run_tests(self, name):
        test_dir = os.path.join('Lib', 'test', 'scipytestdata')
        filename = name + '_data.txt'
        file_path = os.path.join(test_dir, filename)
        func_name = filename.split('_')[0]
        func = getattr(math, func_name)

        f = open(file_path, 'r')
        for line_number, line in enumerate(f):
            line_tokens = [float(fnum) for fnum in line.split()]
            value = line_tokens[0]
            result  = func(*line_tokens[1:])

            self.assertLess(abs(result - value),
                            EPS * (abs(result) + abs(value)),
                            'error in %s, line %d' % (filename, line_number))

@unittest.skipUnless(hasattr(math, 'y0'), "test requires y0")
class y0Test(SciPyTest):
    def test_y0(self):
        self.run_tests('y0')

@unittest.skipUnless(hasattr(math, 'y1'), "test requires y1")
class y1Test(SciPyTest):
    def test_y1(self):
        self.run_tests('y1')

@unittest.skipUnless(hasattr(math, 'yn'), "test requires yn")
class ynTest(SciPyTest):
    def test_yn(self):
        self.run_tests('yn')

@unittest.skipUnless(hasattr(math, 'j0'), "test requires j0")
class j0Test(SciPyTest):
    def test_j0(self):
        self.run_tests('j0')

@unittest.skipUnless(hasattr(math, 'j1'), "test requires j1")
class j1Test(SciPyTest):
    def test_j1(self):
        self.run_tests('j1')

@unittest.skipUnless(hasattr(math, 'jn'), "test requires jn")
class jnTest(SciPyTest):
    def test_jn(self):
        self.run_tests('jn')

@unittest.skipUnless(hasattr(math, 'cbrt'), "test requires cbrt")
class cbrtTest(SciPyTest):
    def test_cbrt(self):
        self.run_tests('cbrt')

if __name__ == '__main__':
    unittest.main()
