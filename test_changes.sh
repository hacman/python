#!/bin/sh

# quit after any failed unit test
set -e

if [ -f ./python ]
then
    python=./python
elif [ -f ./python.exe ]
then
    python=./python.exe
else
    echo no python found
    exit 1
fi

export PYTHONUSEPYTHONDECIMAL=1
${python} Lib/test/test_decimal.py
unset PYTHONUSEPYTHONDECIMAL


for library in Lib/test/test_decimal.py \
               Lib/test/test_math.py \
               Lib/test/test_itertools.py \
               Lib/test/test_peepholer.py \
               Lib/test/test_datetime.py \
               Lib/test/test_time.py \
               Lib/test/test_random.py \
               Lib/test/test_mmap.py \
               Lib/test/test_tempfile.py \
               Lib/test/test_gzip.py \
               Lib/test/test_functools.py \
               Lib/test/test_lzma.py \
               Lib/test/test_lzma_bigmem.py;
do
    echo ${library}
    ${python} ${library}
done

# ${python} Lib/test/test_nis.py # just hangs
